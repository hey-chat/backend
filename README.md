
[![pipeline status](https://gitlab.com/hey-chat/backend/badges/master/pipeline.svg)](https://gitlab.com/hey-chat/backend/-/commits/master) [![coverage report](https://gitlab.com/hey-chat/backend/badges/master/coverage.svg)](https://gitlab.com/hey-chat/backend/-/commits/master)
## Hey-Chat Backend
An API for a Group Chat Application

### Deploy to Development

````
sls deploy --stage=dev
````
