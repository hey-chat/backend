import json
import unittest
from unittest.mock import patch, MagicMock

from chat_event_handler import extract_user_id_for_connection, on_event
from database_handler_interface import DatabaseHandlerInterface


class MockGatewayAPI:

    def __init__(self):
        pass

    def post_to_connection(self, ConnectionId, Data):
        pass


class MockDynamoDbHandler(DatabaseHandlerInterface):

    def get_user_messages_sent(self, connectionId):
        return []

    def get_all_user_connections(self):
        return [{'userId': 'tests', 'sessionId': 'ThxtHdNZjoECFyg='},
                {'userId': 'test4', 'sessionId': 'ThxukeoXDoEAbjA='}]

    def __init__(self):
        pass


class ChatEventHandlerTest(unittest.TestCase):

    def test_extract_user_id_for_connection_verifyUserIdExtracted(self):
        items = [{'userId': 'tests', 'sessionId': 'ThxtHdNZjoECFyg='},
                 {'userId': 'test4', 'sessionId': 'ThxukeoXDoEAbjA='}]
        connection_id = 'ThxtHdNZjoECFyg='
        user_id_expected = 'tests'
        user_id_returned = extract_user_id_for_connection(connection_id, items)
        self.assertEqual(user_id_expected, user_id_returned)

    @patch('chat_event_handler.DynamoDbHandler')
    def test_onevent_onDisconnectEventDeleteUserConnectionIdIsCalled(self, dynamodb_mock):
        connection_id = "TfduTfg6DoECG1A="
        event = {
            "requestContext": {
                "routeKey": "$disconnect",
                "eventType": "DISCONNECT",
                "connectionId": connection_id,
            },
            "isBase64Encoded": False
        }

        dynamodb_instance = DatabaseHandlerInterface()
        dynamodb_instance.delete_user_connectionId = MagicMock()
        dynamodb_mock.return_value = dynamodb_instance
        on_event(event, None)
        dynamodb_instance.delete_user_connectionId.assert_called_with(connection_id)

    @patch('chat_event_handler.DynamoDbHandler')
    def test_onevent_OnConnectEventCheckIfSaveOurConnectionIdIsCalled(self, dynamodb_mock):
        connection_id = "TfduTfg6DoECG1A="
        user_id = "tests"
        event = {
            "requestContext": {
                "routeKey": "$connect",
                "eventType": "CONNECT",
                "connectionId": connection_id,
            },
            "queryStringParameters": {
                "userid": user_id
            },
            "isBase64Encoded": False
        }
        dynamodb_instance = DatabaseHandlerInterface()
        dynamodb_instance.save_user_connectionId = MagicMock()
        dynamodb_mock.return_value = dynamodb_instance
        on_event(event, None)
        dynamodb_instance.save_user_connectionId.assert_called_with(user_id, connection_id)

    @patch('chat_event_handler.DynamoDbHandler')
    @patch('chat_event_handler.client')
    def test_onevent_onMessageEventCheckMessageIsSentToOtherUsers(self, boot_client_mock, dynamodb_mock):
        dynamodb_instance = MockDynamoDbHandler()
        dynamodb_mock.return_value = dynamodb_instance
        mock_gateway_api = MockGatewayAPI()
        mock_gateway_api.post_to_connection = MagicMock()
        boot_client_mock.return_value = mock_gateway_api
        connection_id = 'ThxtHdNZjoECFyg='
        event = {
            "requestContext": {
                "routeKey": "messageReceive",
                "eventType": "MESSAGE",
                "connectionId": connection_id,
                "requestTimeEpoch": 1601153425725,
                "stage": "dev",
                "domainName": "ya3yz1me9g.execute-api.eu-west-1.amazonaws.com",
            },
            "body": "{\"action\": \"messageReceive\", \"message\": \"Hello\"}",
            "isBase64Encoded": False
        }
        on_event(event, None)
        boot_client_mock.assert_called_with('apigatewaymanagementapi',
                                            endpoint_url='https://ya3yz1me9g.execute-api.eu-west-1.amazonaws.com/dev')
        data_being_sent = {'userId': 'tests', 'sentTime': 1601153425725, 'message': 'Hello'}
        mock_gateway_api.post_to_connection.assert_called_with(ConnectionId='ThxukeoXDoEAbjA=',
                                                               Data=json.dumps(data_being_sent).encode('utf-8'))
