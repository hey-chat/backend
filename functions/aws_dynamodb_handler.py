import boto3

from functions.database_handler_interface import DatabaseHandlerInterface


class DynamoDbHandler(DatabaseHandlerInterface):
    TABLE = 'ChatUsers'
    dynamodbTable = None

    def __init__(self):
        self.dynamodbTable = boto3.resource('dynamodb').Table(self.TABLE)

    def save_user_connectionId(self, userId, connectionId):
        item = {
            "userId": userId,
            "sessionId": connectionId,
            "messagesSent": []
        }
        self.dynamodbTable.put_item(Item=item)

    def delete_user_connectionId(self, connectionId):
        item = {
            "sessionId": connectionId
        }
        self.dynamodbTable.delete_item(Key=item)

    def get_all_user_connections(self):
        response = self.dynamodbTable.scan(ProjectionExpression="sessionId, userId")
        return response.get("Items", [])

    def get_user_messages_sent(self, connectionId) -> list:
        response = self.dynamodbTable.get_item(Key={'sessionId':connectionId})
        if 'Item' in response:
            item = response['Item']
            return item['messagesSent']
        return []

    def update_user_sent_messages_list(self, connectionId, sent_messages):
        self.dynamodbTable.update_item(
            Key={
                'sessionId': connectionId
            },
            UpdateExpression="set messagesSent=:a",
            ExpressionAttributeValues={
                ':a': sent_messages
            },
            ReturnValues="UPDATED_NEW"
        )
