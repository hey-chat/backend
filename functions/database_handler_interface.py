class DatabaseHandlerInterface:

    def save_user_connectionId(self, userId, connectionId):
        pass;

    def delete_user_connectionId(self, connectionId):
        pass;

    def get_all_user_connections(self):
        pass

    def get_user_messages_sent(self, connectionId) -> list:
        pass

    def update_user_sent_messages_list(self, connectionId, sent_messages):
        pass