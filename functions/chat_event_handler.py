import json
import logging

from boto3 import client

from functions.aws_dynamodb_handler import DynamoDbHandler



logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def on_connect(event, dynamoDb):
    if 'queryStringParameters' not in event:
        return on_default(event)
    user_id = event['queryStringParameters']['userid']
    logger.info('on connect event received with event: {} for user: {}'.format(event, user_id))
    connection_id = event['requestContext']['connectionId']
    logger.info('on connect called for connectionId: {}'.format(connection_id))
    dynamoDb.save_user_connectionId(user_id, connection_id)
    data = {
        "message": "WebSocket connection to backend is successful"
    }
    return _get_response(200, data)


def on_default(event):
    logger.info('Unrecognized WebSocket action received with event: {}'.format(event))
    data = {
        "message": "Unrecognized WebSocket action."
    }
    return _get_response(400, data)


def on_disconnect(event, dynamoDb):
    logger.info('on dis-connect event received with event: {}'.format(event))
    connection_id = event['requestContext']['connectionId']
    logger.info('on dis-connect called for connectionId: {}'.format(connection_id))
    dynamoDb.delete_user_connectionId(connection_id)
    data = {
        "message": "WebSocket connection to backend is closed"
    }
    return _get_response(200, data)


def on_event(event, context):
    dynamoDb = DynamoDbHandler()
    if event['requestContext']['eventType'] == 'CONNECT':
        return on_connect(event, dynamoDb)
    elif event['requestContext']['eventType'] == 'DISCONNECT':
        return on_disconnect(event, dynamoDb)
    elif event['requestContext']['eventType'] == 'DEFAULT':
        return on_default(event)
    elif event['requestContext']['eventType'] == 'MESSAGE':
        return on_message_received(event, dynamoDb)
    else:
        return on_default(event)


def on_message_received(event, dynamoDb):
    logger.info('on message event received with event: {}'.format(event))
    try:
        items = dynamoDb.get_all_user_connections()
        connection_id = event['requestContext']['connectionId']
        user_id = extract_user_id_for_connection(connection_id, items)

        if not user_id:
            logger.error('no user found for the given connectionID: {}'.format(connection_id))
            return

        body = json.loads(event['body'])
        user_message = body['message']
        message_sent_time = event['requestContext']['requestTimeEpoch']

        user_message_sent = {
            "message": user_message,
            "sentTime": message_sent_time
        }

        logger.debug('fetching sent messages of user: {}'.format(user_id))

        user_sent_messages = dynamoDb.get_user_messages_sent(connection_id)

        logger.debug('user: {} messages sent retreived are of length: {}'.format(user_id, len(user_sent_messages)))

        user_sent_messages.insert(0, user_message_sent)

        if len(user_sent_messages) > 20:
            user_sent_messages.pop()

        logger.info('updating user_messages of size: {}'.format(len(user_sent_messages)))

        dynamoDb.update_user_sent_messages_list(connection_id, user_sent_messages)

        data = {
            "userId": user_id,
            "sentTime": message_sent_time,
            "message": user_message
        }

        for item in items:
            logger.info('item is :{}'.format(item))
            if 'sessionId' in item:
                sessionId = item['sessionId']
                if connection_id == sessionId:
                    # skip the user whose actually sent the message
                    continue
                logging.info('sending message to user: {} of sessionid: {}'.format(item['userId'], item['sessionId']))
                send_message_to_connection(sessionId, data, event)
        return _get_response(200, data)

    except Exception as e:
        data = {
            "message": 'error: {}, to process on_message event'.format(e)
        }
        return _get_response(500, data)


def extract_user_id_for_connection(connection_id, items):
    user_item = [item for item in items if item['sessionId'] == connection_id]
    if not user_item:
        return None
    return user_item[0]['userId']


def send_message_to_connection(connection_id, data, event):
    gatewayapi = client("apigatewaymanagementapi",
                        endpoint_url="https://" + event["requestContext"]["domainName"] +
                                     "/" + event["requestContext"]["stage"])
    response = gatewayapi.post_to_connection(ConnectionId=connection_id,
                                             Data=json.dumps(data).encode('utf-8'))
    logger.info('response is: '.format(response))
    return response


def _get_response(code, data):
    return {
        'statusCode': code,
        'body': json.dumps(data)
    }
